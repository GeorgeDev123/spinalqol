package com.android.spinal.spinalqol.application;

import android.app.Application;
import android.content.Context;

import org.androidannotations.annotations.EApplication;

/**
 * Created by zhizheng on 2/06/16.
 */
@EApplication
public class SpinalQolApplication extends Application {
    private static Context context;
    public void onCreate(){
        super.onCreate();;
        context = getApplicationContext();
    }

    public static Context getAppContext(){
        return context;
    }
}
