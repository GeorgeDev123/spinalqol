package com.android.spinal.spinalqol.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.application.Utils;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by zhi on 27/06/2016.
 */
public class HistoryAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private ArrayList<String> listHistory = new ArrayList<String>();
    private String sizeLabel;
    private String quantityLabel;
    private final String symbol = ": ";

    public HistoryAdapter(Context context, ArrayList<String> listHistory){
        super(context, R.layout.item_history ,listHistory);
        this.mContext = context;
        this.listHistory = listHistory;
    }

    @Override
    public int getCount() {
        return listHistory.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_history, parent,false);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_history_date);
        TextView tvValue = (TextView) convertView.findViewById(R.id.tv_history_time);

        String dateString = listHistory.get(position);
        try {
            tvTitle.setText(Utils.showOnlyDate(dateString));
            tvValue.setText(Utils.showOnlyTime(dateString));
        } catch (ParseException e) {
            return null;
        }

        return convertView;
    }

}
