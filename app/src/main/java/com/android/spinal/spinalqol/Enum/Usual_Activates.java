package com.android.spinal.spinalqol.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 25/06/2016.
 */

@RequiredArgsConstructor
public enum Usual_Activates {
    NO_PROBLEMS_WITH_PERFORMING_MY_USUAL_ACTIVITIES("No problems with performing my usual activities", 1),
    SOME_PROBLEMS_WITH_PERFORMING_MY_USUAL_ACTIVITIES("Some problems with performing my usual activities", 2),
    UNABLE_TO_PERFORM_MY_USUAL_ACTIVITIES("Unable to perform my usual activities", 3),
    UNKNOWN("Unknown", 9);

    @Getter
    private final int choice;
    @Getter
    private String codes;

    private Usual_Activates(String codes, int i) {
        this.codes = codes;
        this.choice = i;
    }

    @Override
    public String toString(){
        return codes;
    }
}
