package com.android.spinal.spinalqol.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by zhi on 27/06/2016.
 */
public class Utils {
    public static String showOnlyDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.parse(date);
        return sdf.format(sdf.parse(date));
    }

    public static String showOnlyTime(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return  sdf.format(sdf.parse(date.split(" ")[1]));
    }
}
