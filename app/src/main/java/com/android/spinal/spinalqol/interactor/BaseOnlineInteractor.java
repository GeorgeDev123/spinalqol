package com.android.spinal.spinalqol.interactor;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.spinal.spinalqol.application.SpinalQolApplication;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;

/**
 * Created by zhizheng on 2/06/16.
 */
@EBean
public class BaseOnlineInteractor {
    @App
    SpinalQolApplication application;
    public boolean isNetworkConnected(){
        Context context = application.getAppContext();
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if(info != null && info.isConnectedOrConnecting()){
            return true;
        }
        return false;
    }
}
