package com.android.spinal.spinalqol.ui.fragment;

import android.widget.ListView;

import com.android.spinal.spinalqol.Adapter.HistoryAdapter;
import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.application.Shared_Pref_;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_history)
public class HistoryFragment extends android.app.Fragment {
    @ViewById(R.id.lv_history)
    ListView history;

    @Pref
    Shared_Pref_ userPrefs;
    ArrayList<String> listHistory;

    @AfterViews
    void setUp() {
        Gson gson = new Gson();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<String> dateStringArray;
        if (userPrefs.answerHistory().exists()) {
            String answers = userPrefs.answerHistory().get();
            dateStringArray = gson.fromJson(answers, new TypeToken<List<String>>() {
            }.getType());
        } else {
            dateStringArray = new ArrayList<>();
        }
        String answers = userPrefs.answerHistory().get();
        dateStringArray = gson.fromJson(answers, new TypeToken<List<String>>() {
        }.getType());

        listHistory = dateStringArray;
        HistoryAdapter historyAdapter = new HistoryAdapter(this.getContext(), listHistory);
        history.setAdapter(historyAdapter);
    }
}
