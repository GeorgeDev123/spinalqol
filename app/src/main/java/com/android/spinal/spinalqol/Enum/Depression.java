package com.android.spinal.spinalqol.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 26/06/2016.
 */
@RequiredArgsConstructor
public enum Depression {
    NO_PAINT_OR_DISCOMFORT("Not anxious or depressed", 1),
    MODERATE_PAIN_OR_DISCOMFORT("Moderately anxious or depressed", 2),
    UNABLE_TO_WASH_OR_DRESS_MYSELF("Extremely anxious or depressed", 3),
    UNKNOWN("Unknown", 9);

    @Getter
    private final int choice;
    @Getter
    private String codes;

    private Depression(String codes, int i) {
        this.codes = codes;
        this.choice = i;
    }

    @Override
    public String toString(){
        return codes;
    }
}
