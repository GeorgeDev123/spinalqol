package com.android.spinal.spinalqol.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 24/06/2016.
 */
@RequiredArgsConstructor
public enum Personal_Care {
    NO_PROBLEMS_WITH_SELF_CARE("No problems with self-care", 1),
    SOME_PROBLEMS_WASHING_OR_DRESSING_MYSELF("Some problems washing or dressing myself", 2),
    UNABLE_TO_WASH_OR_DRESS_MYSELF("Unable to wash or dress myself", 3),
    UNKNOWN("Unknown", 9);

    @Getter
    private final int choice;
    @Getter
    private String codes;

    private Personal_Care(String codes, int i) {
        this.codes = codes;
        this.choice = i;
    }

    @Override
    public String toString(){
        return codes;
    }
}
