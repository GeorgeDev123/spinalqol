package com.android.spinal.spinalqol.ui;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.spinal.spinalqol.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
@EActivity
public class BaseActivity extends AppCompatActivity {
    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    protected void setUp(){
        setSupportActionBar(toolbar);
    }
}
