package com.android.spinal.spinalqol.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 25/06/2016.
 */
@RequiredArgsConstructor
public enum Paint_Discomfort {
    NO_PAINT_OR_DISCOMFORT("No pain or discomfort", 1),
    MODERATE_PAIN_OR_DISCOMFORT("Moderate pain or discomfort", 2),
    UNABLE_TO_WASH_OR_DRESS_MYSELF("Extreme pain or discomfort", 3),
    UNKNOWN("Unknown", 9);

    @Getter
    private final int choice;
    @Getter
    private String codes;

    private Paint_Discomfort(String codes, int i) {
        this.codes = codes;
        this.choice = i;
    }

    @Override
    public String toString(){
        return codes;
    }
}
