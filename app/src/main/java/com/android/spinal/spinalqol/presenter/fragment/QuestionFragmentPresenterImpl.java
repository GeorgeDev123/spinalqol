package com.android.spinal.spinalqol.presenter.fragment;

import com.android.spinal.spinalqol.Entities.Eq_5d;
import com.android.spinal.spinalqol.Enum.Depression;
import com.android.spinal.spinalqol.Enum.Mobility;
import com.android.spinal.spinalqol.Enum.Paint_Discomfort;
import com.android.spinal.spinalqol.Enum.Personal_Care;
import com.android.spinal.spinalqol.Enum.Usual_Activates;
import com.android.spinal.spinalqol.application.Shared_Pref_;
import com.google.gson.Gson;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.EnumSet;

/**
 * Created by zhi on 25/06/2016.
 */
@EBean
public class QuestionFragmentPresenterImpl implements  IQuestionFragmentPresenter {
    @Pref
    Shared_Pref_ userPrefs;
    Gson gson = new Gson();

    private static <E extends Enum<E>> CharSequence[] getEnumString(Class<E> clazz){
        ArrayList<String> enumList = new ArrayList<>();
        for(E en : EnumSet.allOf(clazz)){
            enumList.add(en.toString());
        }
        return enumList.toArray(new CharSequence[enumList.size()]);
    }

    private static <E extends Enum<E>> CharSequence[] getEnumChoices(Class<E> clazz){
        ArrayList<String> enumList = new ArrayList<>();
        for(E en : EnumSet.allOf(clazz)){
            enumList.add(String.valueOf(en.ordinal()));
        }
        return enumList.toArray(new CharSequence[enumList.size()]);
    }

    private Eq_5d answerInitOrRetrived() {
        if (userPrefs.question_answers().exists()) {
            String answers = userPrefs.question_answers().get();
            Eq_5d eq5d = gson.fromJson(answers, Eq_5d.class);
            return eq5d;
        }
        Eq_5d eq5d = new Eq_5d();
        String answersInit = gson.toJson(eq5d);
        userPrefs.question_answers().put(answersInit);
        return eq5d;
    }

    private void answerCreateOrUpdate(Eq_5d eq5d) {
        String jsonEq5d = gson.toJson(eq5d);
        userPrefs.question_answers().put(jsonEq5d);
    }


    public CharSequence[] getMobilityEntries() {
        return getEnumString(Mobility.class);
    }
    public CharSequence[] getMobilityEntriesValues() {
        return getEnumChoices(Mobility.class);
    }
    public String getMobilitySummary(int ordinal) {
        return Mobility.values()[ordinal].getCodes();
    }

    public CharSequence[] getPersonalCareEntries() {
        return getEnumString(Personal_Care.class);
    }
    public CharSequence[] getPersonalCareEntriesValues() {
        return getEnumChoices(Personal_Care.class);
    }
    public String getPersonalCareSummary(int ordinal) {
        return Personal_Care.values()[ordinal].getCodes();
    }


    public String getUsualActivatesSummary(int ordinal) {
        return Usual_Activates.values()[ordinal].getCodes();
    }


    public String getPainDiscomfortSummary(int ordinal) {
        return Paint_Discomfort.values()[ordinal].getCodes();
    }


    public String getDepressionSummary(int ordinal) {
        return Depression.values()[ordinal].getCodes();
    }

    public String getHealthStateTodaySummary(int ordinal) {
        return String.valueOf(ordinal);
    }

    public String getHealthStatePriorSummary(int ordinal) {
        return String.valueOf(ordinal);
    }

    public CharSequence[] getUsualActivatesEntries() {
        return getEnumString(Usual_Activates.class);
    }

    public CharSequence[] getUsualActivatesEntriesValues() {
        return getEnumChoices(Usual_Activates.class);
    }

    public CharSequence[] getPainDiscomfortEntries() {
        return getEnumString(Paint_Discomfort.class);
    }

    public CharSequence[] getPainDiscomfortEntriesValues() {
        return getEnumChoices(Paint_Discomfort.class);
    }

    public CharSequence[] getDepressionEntries() {
        return getEnumString(Depression.class);
    }

    public CharSequence[] getDepressionEntriesValues() {
        return getEnumChoices(Depression.class);
    }

    public void saveMobility(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setMobility(Mobility.values()[ordinal]);
        answerCreateOrUpdate(eq5d);
    }
    public void savePersonalCare(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setPersonalCare(Personal_Care.values()[ordinal]);
        answerCreateOrUpdate(eq5d);
    }
    public void saveUsualActivates(int ordinal){
        Eq_5d eq5d =answerInitOrRetrived();
        eq5d.setUsualActivates(Usual_Activates.values()[ordinal]);
        answerCreateOrUpdate(eq5d);
    }
    public void savePainDiscomfort(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setPaintDiscomfort(Paint_Discomfort.values()[ordinal]);
        answerCreateOrUpdate(eq5d);
    }
    public void saveDepression(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setDepression(Depression.values()[ordinal]);
        answerCreateOrUpdate(eq5d);
    }
    public void saveHealthToday(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setHealthToday(ordinal);
        answerCreateOrUpdate(eq5d);
    }
    public void saveHealthPrior(int ordinal){
        Eq_5d eq5d = answerInitOrRetrived();
        eq5d.setHealthPrior(ordinal);
        answerCreateOrUpdate(eq5d);
    }
}
