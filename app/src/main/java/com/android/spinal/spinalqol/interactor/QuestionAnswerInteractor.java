package com.android.spinal.spinalqol.interactor;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieDataSet;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

/**
 * Created by zhi on 30/06/2016.
 */
@EBean
public class QuestionAnswerInteractor {
    public PieDataSet getMobilityDataset(){
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(4f, 0));
        entries.add(new Entry(8f, 1));
        entries.add(new Entry(6f, 2));
        entries.add(new Entry(12f, 3));

        PieDataSet dataset = new PieDataSet(entries, "");
        return dataset;
    }

    public ArrayList<String> getMobilityEntries(){
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("No problem");
        labels.add("Some problem");
        labels.add("Confined");
        labels.add("Unkown");
        return labels;
    }}
