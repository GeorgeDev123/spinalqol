package com.android.spinal.spinalqol.ui.fragment;

import android.preference.ListPreference;
import android.preference.PreferenceFragment;

import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.presenter.fragment.QuestionFragmentPresenterImpl;
import com.android.spinal.spinalqol.ui.preference.SeekBarPreference;

import org.androidannotations.annotations.AfterPreferences;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PreferenceByKey;
import org.androidannotations.annotations.PreferenceChange;
import org.androidannotations.annotations.PreferenceScreen;

/**
 * Created by zhi on 24/06/2016.
 */
@PreferenceScreen(R.xml.question)
@EFragment
public class QuestionFragment extends PreferenceFragment{
    @Bean
    QuestionFragmentPresenterImpl questionFragmentPresenter;

    @PreferenceByKey(R.string.pref_qst_mobility)
    ListPreference preferenceMobility;
    @PreferenceByKey(R.string.pref_qst_personal_care)
    ListPreference preferencePersonalCare;
    @PreferenceByKey(R.string.pref_qst_usual_activates)
    ListPreference preferenceUsualActivates;
    @PreferenceByKey(R.string.pref_qst_pain_discomfort)
    ListPreference preferencePainDiscomfort;
    @PreferenceByKey(R.string.pref_qst_anxiety_depression)
    ListPreference preferenceAnxietyDepression;
    @PreferenceByKey(R.string.pref_qst_health_state_today)
    SeekBarPreference preferenceHealthStateToday;
    @PreferenceByKey(R.string.qst_health_state_prior)
    SeekBarPreference preferenceHealthSatePrior;

    @AfterPreferences
    void initPrefs() {

        preferenceMobility.setEntries(questionFragmentPresenter.getMobilityEntries());
        preferenceMobility.setEntryValues(questionFragmentPresenter.getMobilityEntriesValues());

        preferencePersonalCare.setEntries(questionFragmentPresenter.getPersonalCareEntries());
        preferencePersonalCare.setEntryValues(questionFragmentPresenter.getPersonalCareEntriesValues());

        preferenceUsualActivates.setEntries(questionFragmentPresenter.getUsualActivatesEntries());
        preferenceUsualActivates.setEntryValues(questionFragmentPresenter.getUsualActivatesEntriesValues());

        preferencePainDiscomfort.setEntries(questionFragmentPresenter.getPainDiscomfortEntries());
        preferencePainDiscomfort.setEntryValues(questionFragmentPresenter.getPainDiscomfortEntriesValues());

        preferenceAnxietyDepression.setEntries(questionFragmentPresenter.getDepressionEntries());
        preferenceAnxietyDepression.setEntryValues(questionFragmentPresenter.getDepressionEntriesValues());


    }

    @PreferenceChange(R.string.pref_qst_mobility)
    void mobilityPreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferenceMobility.setSummary(questionFragmentPresenter.getMobilitySummary(index));
        questionFragmentPresenter.saveMobility(index);
    }
    @PreferenceChange(R.string.pref_qst_personal_care)
    void personalCarePreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferencePersonalCare.setSummary(questionFragmentPresenter.getPersonalCareSummary(index));
        questionFragmentPresenter.savePersonalCare(index);
    }
    @PreferenceChange(R.string.pref_qst_usual_activates)
    void usualActivatesPreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferenceUsualActivates.setSummary(questionFragmentPresenter.getUsualActivatesSummary(index));
        questionFragmentPresenter.saveUsualActivates(index);
    }
    @PreferenceChange(R.string.pref_qst_pain_discomfort)
    void painDiscomfortPreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferencePainDiscomfort.setSummary(questionFragmentPresenter.getPainDiscomfortSummary(index));
        questionFragmentPresenter.savePainDiscomfort(index);
    }
    @PreferenceChange(R.string.pref_qst_anxiety_depression)
    void anxietyDepressionPreferenceChange(String newValue){
        int index = Integer.parseInt(newValue);
        preferenceAnxietyDepression.setSummary(questionFragmentPresenter.getDepressionSummary(index));
        questionFragmentPresenter.saveDepression(index);
    }
    @PreferenceChange(R.string.pref_qst_health_state_today)
    void preferenceHealthStateTodayChange(int newValue){
        preferenceHealthStateToday.setSummary(questionFragmentPresenter.getHealthStateTodaySummary(newValue));
        questionFragmentPresenter.saveHealthToday(newValue);
    }
    @PreferenceChange(R.string.qst_health_state_prior)
    void preferenceHealthSatePriorChange(int newValue){
        preferenceHealthSatePrior.setSummary(questionFragmentPresenter.getHealthStatePriorSummary(newValue));
        questionFragmentPresenter.saveHealthPrior(newValue);
    }

}
