package com.android.spinal.spinalqol.Entities;

import com.android.spinal.spinalqol.Enum.Depression;
import com.android.spinal.spinalqol.Enum.Mobility;
import com.android.spinal.spinalqol.Enum.Paint_Discomfort;
import com.android.spinal.spinalqol.Enum.Personal_Care;
import com.android.spinal.spinalqol.Enum.Usual_Activates;

import lombok.Data;

/**
 * Created by zhi on 24/06/2016.
 */
@Data
public class Eq_5d {
    Mobility mobility = Mobility.UNKNOWN;
    Personal_Care personalCare = Personal_Care.UNKNOWN;
    Usual_Activates usualActivates = Usual_Activates.UNKNOWN;
    Paint_Discomfort paintDiscomfort = Paint_Discomfort.UNKNOWN;
    Depression depression = Depression.UNKNOWN;
    int healthToday = 999;
    int healthPrior = 999;
}
