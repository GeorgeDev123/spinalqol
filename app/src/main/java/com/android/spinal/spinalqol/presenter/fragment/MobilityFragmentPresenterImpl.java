package com.android.spinal.spinalqol.presenter.fragment;

import com.android.spinal.spinalqol.Entities.Eq_5d;
import com.android.spinal.spinalqol.application.Shared_Pref_;
import com.android.spinal.spinalqol.interactor.QuestionAnswerInteractor;
import com.android.spinal.spinalqol.ui.fragment.MobilityFragment;
import com.google.gson.Gson;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by zhi on 29/06/2016.
 */
@EBean(scope = EBean.Scope.Singleton)
public class MobilityFragmentPresenterImpl {
    @Pref
    Shared_Pref_ userPrefs;

    MobilityFragment mobilityFragment;

    @Bean
    QuestionAnswerInteractor interactor;

    Gson gson = new Gson();

    public void setView(MobilityFragment fragment) {
        this.mobilityFragment = (MobilityFragment) fragment;
    }
    public void initView(){
        String answers = userPrefs.question_answers().get();
        Eq_5d eq5d = gson.fromJson(answers, Eq_5d.class);
        mobilityFragment.onSetChart(interactor.getMobilityEntries(),interactor.getMobilityDataset(),eq5d.getMobility().toString());
    }




}
