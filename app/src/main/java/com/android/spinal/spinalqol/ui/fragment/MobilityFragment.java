package com.android.spinal.spinalqol.ui.fragment;

import android.graphics.Color;
import android.widget.TextView;

import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.application.Shared_Pref_;
import com.android.spinal.spinalqol.presenter.fragment.MobilityFragmentPresenterImpl;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;

@EFragment(R.layout.fragment_mobility)
public class MobilityFragment extends android.app.Fragment {
    @ViewById(R.id.chart)
    PieChart pieChart;
    @Pref
    Shared_Pref_ userPrefs;
    @Bean
    MobilityFragmentPresenterImpl presenter;
    @ViewById(R.id.tv_chart)
    public TextView tvChart;

    @AfterViews
    void setUp (){
        presenter.setView(this);
        presenter.initView();
    }
    public void onSetChart(ArrayList<String> labels, PieDataSet dataset, String answer){
        PieData data = new PieData(labels, dataset);
        pieChart.setData(data);
        pieChart.setDescription(null);  // set the description

        dataset.setValueLineColor(Color.RED);
        dataset.setValueTextSize(11);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        pieChart.setDescriptionColor(dataset.getColor(1));
    }
}
