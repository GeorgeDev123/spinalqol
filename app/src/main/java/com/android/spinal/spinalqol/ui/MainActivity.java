package com.android.spinal.spinalqol.ui;

import android.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.application.Shared_Pref_;
import com.android.spinal.spinalqol.ui.fragment.FinishFragment;
import com.android.spinal.spinalqol.ui.fragment.FinishFragment_;
import com.android.spinal.spinalqol.ui.fragment.HistoryFragment;
import com.android.spinal.spinalqol.ui.fragment.HistoryFragment_;
import com.android.spinal.spinalqol.ui.fragment.MobilityFragment;
import com.android.spinal.spinalqol.ui.fragment.MobilityFragment_;
import com.android.spinal.spinalqol.ui.fragment.QuestionFragment;
import com.android.spinal.spinalqol.ui.fragment.QuestionFragment_;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Pref
    Shared_Pref_ userPrefs;

    QuestionFragment questionFragment;
    HistoryFragment historyFragment;
    FinishFragment finishedFragment;
    MobilityFragment mobilityFragment;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;


    NavigationView navigationView;
    MenuItem menuSubmit;

    public NavigationView.OnNavigationItemSelectedListener navigationItemSelected() {
        return new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                //Closing drawer on item click
                drawerLayout.closeDrawers();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:
                        questionFragment = QuestionFragment_.builder().build();
                        ft.replace(R.id.frame_qst, questionFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(true);
                        break;
                    // For rest of the options we just show a toast on click
                    case R.id.history:
                        historyFragment = HistoryFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, historyFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("History");
                        break;
                    case R.id.chart_mobility:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Mobility analysis");
                        break;
                    case R.id.chart_depression:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Depression analysis");
                        break;
                    case R.id.chart_personal_care:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Personal care analysis");
                        break;
                    case R.id.chart_usual_activates:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Activates analysis");
                        break;
                    case R.id.chart_paint_discomfort:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Paint discomfort analysis");
                        break;
                    case R.id.chart_health_today:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Health today analysis");
                        break;
                    case R.id.chart_health_prior:
                        mobilityFragment = MobilityFragment_.builder().build();
                        ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_qst, mobilityFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        menuSubmit.setVisible(false);
                        setTitle("Health prior analysis");
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;
                }
                return true;
            }
        };
    }
    @AfterViews
    protected void setUp() {
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        super.setUp();
        navigationView.setNavigationItemSelectedListener(navigationItemSelected());
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        questionFragment = QuestionFragment_.builder().build();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame_qst, questionFragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menuSubmit = menu.findItem(R.id.action_submit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.action_submit: {
                saveHistory();
                FragmentTransaction ft;
                finishedFragment = FinishFragment_.builder().build();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_qst, finishedFragment);
                ft.commit();
                menuSubmit.setVisible(false);
                break;
            }
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveHistory(){
        Gson gson = new Gson();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<String> dateStringArray;
        String sCertDate = dateFormat.format(new Date());
        if(userPrefs.answerHistory().exists()){
            String answers = userPrefs.answerHistory().get();
            dateStringArray = gson.fromJson(answers, new TypeToken<List<String>>(){}.getType());
            dateStringArray.add(sCertDate);
        }
        else{
            dateStringArray = new ArrayList<>();
            dateStringArray.add(sCertDate);
        }
        String jsonText = gson.toJson(dateStringArray);
        userPrefs.answerHistory().put(jsonText);
    }

}
