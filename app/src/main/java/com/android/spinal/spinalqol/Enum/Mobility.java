package com.android.spinal.spinalqol.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhi on 24/06/2016.
 */
@RequiredArgsConstructor
public enum Mobility {
    NO_PROBLEMS_IN_WALKING_ABOUT("No problems in walking about", 1),
    SOME_PROBLEMS_IN_WALKING_ABOUT("Some problems in walking about", 2),
    CONFINED_TO_BED("Confined to bed", 3),
    UNKNOWN("Unknown", 9);

    @Getter
    private final int choice;
    @Getter
    private String codes;

    private Mobility(String codes, int i) {
        this.codes = codes;
        this.choice = i;
    }

    @Override
    public String toString(){
        return codes;
    }
}
