package com.android.spinal.spinalqol.ui;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import com.android.spinal.spinalqol.R;
import com.android.spinal.spinalqol.presenter.LoginPresenter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * A login screen that offers login via email/password.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById(R.id.code)
    AutoCompleteTextView code;

    @ViewById(R.id.login_progress)
    ProgressBar progressBar;

    @Click(R.id.sign_in)
    void onLogin(){
        presenter.login(code.getText().toString());
    }
    @Bean
    LoginPresenter presenter;


    @AfterViews
    void onAfterView(){
        presenter.setView(this);
    }

    public void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress(){
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void setCodeError(){
        code.setError(getString(R.string.code_error));
    }

}

