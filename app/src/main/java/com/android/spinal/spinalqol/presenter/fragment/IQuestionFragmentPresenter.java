package com.android.spinal.spinalqol.presenter.fragment;

/**
 * Created by zhi on 25/06/2016.
 */
public interface IQuestionFragmentPresenter {
    CharSequence[] getMobilityEntries();
    CharSequence[] getMobilityEntriesValues();
    CharSequence[] getPersonalCareEntries();
    CharSequence[] getPersonalCareEntriesValues();
    CharSequence[] getUsualActivatesEntries();
    CharSequence[] getUsualActivatesEntriesValues();
    CharSequence[] getPainDiscomfortEntries();
    CharSequence[] getPainDiscomfortEntriesValues();
    CharSequence[] getDepressionEntries();
    CharSequence[] getDepressionEntriesValues();

    String getMobilitySummary(int ordinal);
    String getPersonalCareSummary(int ordinal);
    String getUsualActivatesSummary(int ordinal);
    String getPainDiscomfortSummary(int ordinal);
    String getDepressionSummary(int ordinal);
    String getHealthStateTodaySummary(int ordinal);
    String getHealthStatePriorSummary(int ordinal);

    void saveMobility(int ordinal);
    void savePersonalCare(int ordinal);
    void saveUsualActivates(int ordinal);
    void savePainDiscomfort(int ordinal);
    void saveDepression(int ordinal);
    void saveHealthToday(int ordinal);
    void saveHealthPrior(int ordinal);


}
