package com.android.spinal.spinalqol.application;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by zhi on 27/06/2016.
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface Shared_Pref {

    String question_answers();
    String answerHistory();

}
