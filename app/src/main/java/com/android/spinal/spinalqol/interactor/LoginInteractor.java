package com.android.spinal.spinalqol.interactor;

import com.android.spinal.spinalqol.presenter.LoginPresenter;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

/**
 * Created by zhizheng on 2/06/16.
 */

@EBean
public class LoginInteractor extends BaseOnlineInteractor {

    public boolean login(String code){
        //// TODO: 2/06/16
        if(!super.isNetworkConnected()){
            return false;
        }
        return true;
    }
}
