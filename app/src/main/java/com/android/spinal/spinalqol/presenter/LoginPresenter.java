package com.android.spinal.spinalqol.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.android.spinal.spinalqol.interactor.LoginInteractor;
import com.android.spinal.spinalqol.ui.LoginActivity;
import com.android.spinal.spinalqol.ui.MainActivity_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

/**
 * Created by zhizheng on 31/05/16.
 */
@EBean(scope = EBean.Scope.Singleton)
public class LoginPresenter implements Presenter{

    private LoginActivity loginActivity;

    @Bean
    LoginInteractor loginInteractor;

    @Override
    public void setView(AppCompatActivity activity) {
        this.loginActivity = (LoginActivity) activity;
    }
    public void login(String code){
        if(loginActivity != null){
            loginActivity.showProgress();
            if(code == null || TextUtils.isEmpty(code)){
                loginActivity.setCodeError();
                loginActivity.hideProgress();
                return;
            }
            loginInteractor.login(code);
            loginActivity.hideProgress();
            Intent myIntent = new Intent(loginActivity.getBaseContext(), MainActivity_.class);
            loginActivity.startActivity(myIntent);
        }
    }
}
